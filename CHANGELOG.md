# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

<!-- markdownlint-disable MD024 -->

<!-- next-version -->
## [Unreleased] - ReleaseDate

### Technical

* bump Rust from `1.75` to `1.78`
* update dependencies

[Unreleased]: https://gitlab.com/pinage404/git-gamble/-/compare/version/2.7.0...HEAD

## [2.7.0] - 2024-02-08

### Security

* bump dependencies to fix the high-severity security vulnerability [`RUSTSEC-2024-0006`](https://rustsec.org/advisories/RUSTSEC-2024-0006.html)

### Features

* add [`human-panic`](https://crates.io/crates/human-panic), a tool that encourages users who experience a crash to report it

### Documentation

* add [a website with all the documentation](https://pinage404.gitlab.io/git-gamble/)
* add [slides about the theory](https://pinage404.gitlab.io/git-gamble/slides_theory)
* add [slides about the demo](https://pinage404.gitlab.io/git-gamble/slides_demo)

### Technical

* add [`compose.yml`](./compose.yml) file to help to debug the CI
* bump container image from Debian Buster to Bookworm
* bump Rust from `1.73` to `1.75`
* bump dependencies

[2.7.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/2.6.0...version/2.7.0

## [2.6.0] - 2023-11-05

### Features

* when generating shell completion, if the user don't give the target shell, try to determine the current shell from the environment or default to Bash

### Documentation / Distribution

<!-- markdownlint-disable-next-line MD039 MD045 -->
* add [a section](./README.md#do-you-like-this-project-) to invite people to add [a star on GitLab ![](https://img.shields.io/gitlab/stars/pinage404/git-gamble?style=social)](https://gitlab.com/pinage404/git-gamble) or open [open an issue](https://gitlab.com/pinage404/git-gamble/-/issues) to give feedback
* add [Nix's template](./README.md#nix-nixos)
* when installing using Nix, add [a section to avoid rebuilding using Cachix](./README.md#cachix)
* document [how to download latest main version of AppImage](./README.md#appimage)
* add [a repository with several examples with several languages](./README.md#usage-examples)
* improve [how to use's section](./README.md#how-to-use-) ; Contribution by [@mavomo (Michelle Avomo)](https://gitlab.com/mavomo) in [MR !3](https://gitlab.com/pinage404/git-gamble/-/merge_requests/3)
* fix [Windows / Chocolatey installation](./README.md#windows--chocolatey), ask users to install git manually instead of having an automatic dependency which was not working
* fix GitLab's URL on crates.io
* fix Nix overlay

### Contributing

* document [how to do refactoring session](README.md#refactoring-session)

### Technical

* test refactoring to help readability
* improve release binary size thanks to [this repository](https://github.com/johnthagen/min-sized-rust)
  * from `952 704 octets` to `739 712 octets`
* bump container base images
* bump Kaniko
* bump Rust from `1.60` to `1.73`
* bump dependencies
* make [compilation faster by combining all tests in a single test binary](https://endler.dev/2020/rust-compile-times/#combine-all-integration-tests-in-a-single-binary)
* add [VS Code's tasks](https://code.visualstudio.com/Docs/editor/tasks) to [use git-gamble inside VS Code](./.vscode/tasks.json)
* use [Mask](https://github.com/jacobdeichert/mask) to [execute commands](./maskfile.md)
* generate shell completions using the `git-gamble` binary itself instead of at build time

[2.6.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/2.5.0...version/2.6.0

## [2.5.0] - 2022-07-03

### Features

* add [custom `git-gamble` hooks](README.md#hooks) : `pre-gamble` and `post-gamble`

### Documentation / Distribution

* add [demo](docs/src/usage/demo/README.md)
* document [how to upgrade when installing using Homebrew](README.md#upgrade)
* add Nix Flake's `overlays`
* fix changelog in release

### Contribution

* setup [Gitpod](README.md#gitpod) to help contributions

### Technical

* bump Rust `edition` from `2018` to `2021`
* replace `structopt` by `clap` which includes the same features
* add test Nix build in pipeline
* add Nix formatter
* fix coverage report [![coverage report](https://gitlab.com/pinage404/git-gamble/badges/main/coverage.svg)](https://gitlab.com/pinage404/git-gamble/-/commits/main)
* bump [Rust VSCode's Extension Pack](https://marketplace.visualstudio.com/items?itemName=pinage404.rust-extension-pack) that replace [Rust](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust) by [`rust-analyzer`](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer)
* replace [`spectral`](https://crates.io/crates/spectral), which is unmaintained, by [`speculoos`](https://crates.io/crates/speculoos) his fork successor

[2.5.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/2.4.0...version/2.5.0

## [2.4.0] - 2022-04-30

### Features

* add [`--edit` option](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---edit) to edit the commit message when committing which is passed to [`git commit`](https://git-scm.com/docs/git-commit) command
* add [`--fixup` option](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---fixupamendrewordltcommitgt) and [`--squash` option](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---squashltcommitgt) which are passed to [`git commit`](https://git-scm.com/docs/git-commit) command

### Documentation / Distribution

* add [Nix Flake installation instructions](README.md#nix-nixos)

### Technical

* bump some dependencies
* use Nix Flake to generate environment with `nix-shell` or `nix develop`
* use [`devshell`](https://github.com/numtide/devshell) to have commands shortcuts
* bump Rust from `1.55` to `1.60`
* ignore audit dependency because there is no safe upgrade available
* release only on main

[2.4.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/2.3.0...version/2.4.0

## [2.3.0] - 2021-10-23

### Documentation

* auto updated `git gamble generate-shell-completions --help` in [README](README.md#shells-completions)

### Distribution

* publish Linux and Windows binaries in [releases](https://gitlab.com/pinage404/git-gamble/-/releases)

### Technical

* fix [`markdownlint`](https://github.com/DavidAnson/markdownlint) issues
* hardcode TOC generated by [Markdown All In One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) instead of the one generated by GitLab
* faster CI by removing useless job
* generate test coverage

[2.3.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/2.2.1...version/2.3.0

## [2.2.1] - 2021-10-03

* Fix deployment on crate.io

[2.2.1]: https://gitlab.com/pinage404/git-gamble/-/compare/version/2.2.0...version/2.2.1

## [2.2.0] - 2021-10-02

### Features

* Wrap `git-gamble --help` based on the terminal size

### Documentation

* auto updated `git-gamble --help` in [README](README.md#usage)

### Technical

* Improve release binary size thanks to [this post](https://arusahni.net/blog/2020/03/optimizing-rust-binary-size.html)
  * from `5 934 760 octets` (~6Mo) to `772 416 octets` (<1Mo)

[2.2.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/2.1.0...version/2.2.0

## [2.1.0] - 2021-10-01

### Features

* improve `git-gamble --help`
  * add color
  * reorder `--fail`, `--pass` and `--refactor` flags
  * add description of the program
  * add link to the repository
* generate shell completions using the subcommand `git gamble generate-shell-completions`

### Documentation

* add [section that help to debug](README.md#debug) while crafting git-gamble itself

### Technical

* update [`release-cli`](https://gitlab.com/gitlab-org/release-cli)
* explicitly set `rustc` as a dependency

[2.1.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/2.0.0...version/2.1.0

## [2.0.0] - 2021-09-25

### Packages repositories

* **⚠️ BREAKING CHANGE ⚠️** remove BinTray package publishing

### Features

* add [`--no-verify` option](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---no-verify) to skip [githooks](https://git-scm.com/docs/githooks) that is passed to [`git commit`](https://git-scm.com/docs/git-commit) command ; Contribution by [@korrat (Markus Haug)](https://gitlab.com/korrat) in [MR !2](https://gitlab.com/pinage404/git-gamble/-/merge_requests/2)

### Documentation / Distribution

* improve [Nix / NixOS installation instructions](README.md#nix-nixos)
* add shell completions for [Nix / NixOS](README.md#nix-nixos)
* add man page for [Nix / NixOS](README.md#nix-nixos)
* improve [AppImage installation instructions](README.md#appimage)

### Technical

* smaller container image in CI
* upload only required files to crates.io, thanks to [`cargo-diet`](https://github.com/the-lean-crate/cargo-diet)
* bump Rust from `1.50` to `1.55`
* bump some dependencies
* remove some linters warnings
* allow `git-gamble` to run inside `nix-shell`
* explicitly set `cargo` as a dependency

### Others

* rename default branch from `master` to `main`

[2.0.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/1.3.0...version/2.0.0

## [1.3.0] - 2021-03-12

### Technical

#### Packages repositories

Packages repositories will be [moved on GitLab](https://gitlab.com/pinage404/git-gamble/-/packages) because [JFrog will shutdown BinTray](https://jfrog.com/blog/into-the-sunset-bintray-jcenter-gocenter-and-chartcenter/)

Please **follow [installation instructions](README.md#how-to-install-)** for your specific system

* _Affected_ by this changement
  * **⚠️ Automatic update won't work in the future ⚠️**
    * [Debian](README.md#debian)
    * [Windows / Chocolatey](README.md#windows-chocolatey)
  * There is not automatic update system at the moment, you already have to manually update at each new version
    * [AppImage](README.md#appimage)
* _Not affected_ by this changement
  * Mac OS X / Homebrew
  * Nix / NixOS
  * Cargo

This will be a kind of **⚠️ BREAKING CHANGE ⚠️** : `2.0.0` won't be on BinTray

#### Others

* bump image's version for the CI's container
* bump Kaniko's version to build the CI's container

[1.3.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/1.2.0...version/1.3.0

## [1.2.0] - 2021-02-20

### Added

* installation for [Nix / NixOS](./README.md#nix-nixos)

### Technical

* remove warning at compile time (while using `cargo test`) ; Contribution by [@bachrc](https://gitlab.com/bachrc) in [MR !1](https://gitlab.com/pinage404/git-gamble/-/merge_requests/1)
* improve the speed of the CI
* improve the way to set up the development environment
* bump Rust to `1.50`
* bump dependencies

[1.2.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/1.1.0...version/1.2.0

## [1.1.0] - 2020-05-25

### Added

* custom logo
* shells completions
  * automatically installed with Debian and Homebrew packages
  * only for Bash, Fish, ZSH

[1.1.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/1.0.0...version/1.1.0

## [1.0.0] - 2020-04-26

Initial project

With the minimum functionalities required for real life use

Installable on Linux, Mac OS X and Windows

[1.0.0]: https://gitlab.com/pinage404/git-gamble/-/compare/077c3e01064ee7923917a919ccb0cb581653f4c5...version/1.0.0
