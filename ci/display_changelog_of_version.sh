#!/usr/bin/env sh

VERSION="$1"

PROJECT_ROOT=$(dirname "$(realpath $0)")/..

. "${PROJECT_ROOT}/script/display_file_between_bounds.sh"

if [ -n "${VERSION}" ]; then
	display_file_between_bounds "## \[${VERSION}]" "^\[${VERSION}]" <CHANGELOG.md
else
	cat <<EOF
Display the changelog's section for a specific <VERSION>

Usage:
	$0 <VERSION>

Args:
	<VERSION>
		The version to display changelog

Exemple:
	$0 1.2.0
EOF
	exit 1
fi
