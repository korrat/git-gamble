FROM rust:1.78-slim-bookworm

RUN apt-get --quiet=2 update \
	&& apt-get --quiet=2 install --no-install-recommends \
	git \
	&& apt-get --quiet=2 clean \
	&& rm --force --recursive /var/lib/apt/lists/*
