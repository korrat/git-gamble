FROM alpine:3.18

RUN apk add \
	coreutils \
	git \
	bash \
	shunit2

USER 1000

CMD shunit2
