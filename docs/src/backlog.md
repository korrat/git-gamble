# Backlog

- [when revert -> `git clean` #3](https://gitlab.com/pinage404/git-gamble/-/issues/3)
- `git workspace` support
  - `git update-ref` should contain an unique identifier to the workspace
    - branche name ?
    - folder path ?
- gamble hooks
  - branch based developement
    - `git commit --fixup`
    - `git rebase --autosquash`
  - optional hook to revert if not gambled in a delay
    - `git-time-keeper`
      - document
      - package
      - distribute
- like git, flags & options & arguments should be retrieved from CLI or environment variable or config's file
  - re-use `git config` to store in file ?
  - repository level config using direnv and environment variable ?
- [stash instead of revert ?](https://rachelcarmena.github.io/2018/11/13/test-driven-programming-workflows.html#my-proposal-of-tcr-variant)
- shell completion
  - in the package ?
    - [ ] Cargo
    - [ ] AppImage
    - [ ] Chocolatey
  - for `git gamble` not only `git-gamble`
- https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html
- https://gitlab.com/gitlab-org/security-products/analyzers
- https://gitlab.com/gitlab-org/security-products/ci-templates
- https://medium.com/@tdeniffel/tcr-variants-test-commit-revert-bf6bd84b17d3
- https://svgfilters.com/
- https://github.com/sourcefrog/cargo-mutants
- https://github.com/llogiq/mutagen

## Distribution / publishing backlog

- OS distribution
  - Linux
    - RPM
      - [`cargo-rpm`](https://crates.io/crates/cargo-rpm)
      - `nix bundle`
    - make AppImage updatable
      - bintray-zsync|pinage404|git-gamble-AppImages|git-gamble|git-gamble-\_latestVersion-x86_64.AppImage.zsync
    - SnapCraft ?
      - [snapcraft's docs about rust](https://snapcraft.io/docs/rust-applications)
    - FlatPak ?
    - [`fpm` : tool that help to generate to several packages](https://github.com/jordansissel/fpm)
    - document AppImage with firejail ?
    - document AppImage with bubblewrap ?
    - [Open Build Service](https://build.opensuse.org/) seems to be a service that build for every Linux targets
      - it can be [use](https://en.opensuse.org/openSUSE:Build_Service_Clients)
        - thougth REST
          - badly documented
        - with `ocs` a CLI who seems to be a VCS like SVN (commit = commit + push)
          - who use the REST API under the hood
  - Mac OS X
    - experiment [GitLab Runner on Mac OS X](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html)
      - run tests
      - precompile
  - try to distribute OS less binaries
    - [`x86_64-unknown-none`](https://doc.rust-lang.org/beta/rustc/platform-support/x86_64-unknown-none.html)
  - Container Image (Docker) ?
  - [Awesome Rust Deployment](https://github.com/rust-unofficial/awesome-rust#deployment)
  - [Awesome Rust Platform Specific](https://github.com/rust-unofficial/awesome-rust#platform-specific)
- permanent URL to download latest version
  - symlinks to URL containing the version name ?
  - using GitLab Pages ?
- versioned Homebrew Formula
  - Use [Cargo-Release](https://github.com/sunng87/cargo-release/blob/master/docs/faq.md#how-do-i-update-my-readme-or-other-files) to bump version
  - how to update sha256 in the versioned file ?

### CheckList

- [ ] package in local
- [ ] package in CI
- [ ] upload
- [ ] make public available
- [ ] complete how to install
- [ ] manual test
- [ ] update backlog

## Technical improvement opportunities

- licence check
  - cargo license
  - cargo deny
- smaller binary
  - cargo bloat
  - cargo deps
- refactor to [iterate over `Shell` `enum`'s values instead of having an hard-coded array](https://gitlab.com/pinage404/git-gamble/-/blob/6bd96ce884afe677ad50671e95631609962ca74a/src/build.rs#L26)
