# Contributing

[![License ISC](https://img.shields.io/crates/l/git-gamble.svg)](https://gitlab.com/pinage404/git-gamble/blob/main/LICENSE)

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](https://gitlab.com/pinage404/git-gamble/-/blob/main/code_of_conduct.md)

Any contributions ([feedback, bug report](https://gitlab.com/pinage404/git-gamble/-/issues), [merge request](https://gitlab.com/pinage404/git-gamble/-/merge_requests) ...) are welcome

<!-- markdownlint-disable MD045 -->
[![Open an issue](https://img.shields.io/gitlab/issues/all/pinage404%2Fgit-gamble?logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/issues)
[![Open a merge request](https://img.shields.io/gitlab/merge-requests/all/pinage404%2Fgit-gamble?logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/merge_requests)
<!-- markdownlint-restore -->

Respect the [code of conduct](https://gitlab.com/pinage404/git-gamble/-/blob/main/code_of_conduct.md)

Follow [Keep a Changelog](https://keepachangelog.com/en/1.1.0/)
