# Add to a package repository

Adding package to the `X` package repository where the `X` package repository is e.g. Nixpgks, Debian, Homebrew, Chocolatey ...

Feel free to do it, we don't plan to do it at the moment, it's too long to learn and understand how each of them works

<!-- markdownlint-disable-next-line MD039 MD045 -->
If you do it, please [file an issue ![](https://img.shields.io/gitlab/issues/all/pinage404%2Fgit-gamble?logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/issues/new) or [open an MR ![](https://img.shields.io/gitlab/merge-requests/all/pinage404%2Fgit-gamble?logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/merge_requests/new) to update the documentation
