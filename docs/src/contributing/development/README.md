# Development

<!-- markdownlint-disable-next-line MD039 MD045 -->
The following sections will help you to contribute with a [merge request ![](https://img.shields.io/gitlab/merge-requests/all/pinage404%2Fgit-gamble?logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/merge_requests)

[![dependency status](https://deps.rs/crate/git-gamble/2.7.0/status.svg)](https://deps.rs/crate/git-gamble/2.7.0)
[![coverage report](https://gitlab.com/pinage404/git-gamble/badges/main/coverage.svg)](https://gitlab.com/pinage404/git-gamble/-/commits/main)
[![CodeScene Code Health](https://codescene.io/projects/47370/status-badges/code-health)](https://codescene.io/projects/47370)
[![pipeline status](https://img.shields.io/gitlab/pipeline/pinage404/git-gamble?label=pipeline&logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/pipelines)
[![AppVeyor for Homebrew status](https://ci.appveyor.com/api/projects/status/qrd9p11ec2kbt1xs?svg=true)](https://ci.appveyor.com/project/pinage404/git-gamble)
[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

<!-- markdownlint-disable-next-line MD045 -->
The coverage report is low [![(coverage report with low percent)](https://gitlab.com/pinage404/git-gamble/badges/main/coverage.svg)](https://gitlab.com/pinage404/git-gamble/-/commits/main) because there are mainly end-to-end tests that are not taken into account
