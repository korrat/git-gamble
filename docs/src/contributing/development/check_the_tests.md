# Check that the code is properly tested

To verify that the code is actually tested by the tests, run the following command :

```sh
mask test mutants
```
