# Refactoring session

At the beginning of your session, just run the following command :

```sh
cargo watch -- git gamble --refactor
```

This is just [TCR](../../theory.md#tcr) (without [TDD](../../theory.md#tdd))
