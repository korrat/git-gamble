# Development Setup

## Gitpod

You can contribute with just your web browser, directly from Gitpod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/pinage404/git-gamble)

## Nix

The easiest way to get everything you need is to use [Nix](https://nixos.org/)

This project is [![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

1. Install [Nix](https://github.com/DeterminateSystems/nix-installer)
1. Install [DirEnv](https://direnv.net/)
1. Let `direnv` automagically set up the environment by executing the following command in the project directory

   ```shell
   direnv allow
   ```

### Troubleshooting

If you get an error like this one when entering in the folder

```text
direnv: loading ~/Project/git-gamble/.envrc
direnv: using nix
direnv: using cached derivation
direnv: eval /home/pinage404/Project/git-gamble/.direnv/cache-.336020.2128d0aa28e
direnv: loading ./script/setup_variables.sh
   Compiling git-gamble v2.4.0-alpha.0 (/home/pinage404/Project/git-gamble)
direnv: ([/nix/store/19arfqh2anf3cxzy8zsiqp08xv6iq6nl-direnv-2.29.0/bin/direnv export fish]) is taking a while to execute. Use CTRL-C to give up.
error: linker `cc` not found
  |
  = note: No such file or directory (os error 2)

error: could not compile `git-gamble` due to previous error
```

Just run the following command to fix it

```sh
direnv reload
```

Help wanted to permanently fix this

## Manual

- Install [Rustup](https://rustup.rs/)
- Install [everything that should normally be installed](https://gitlab.com/pinage404/git-gamble/-/blob/main/.envrc) by [DirEnv](https://direnv.net/)
