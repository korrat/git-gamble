<!-- markdownlint-disable-next-line MD041 -->
## Improving installation

Installation is currently not really convenient, so [contributions are welcome](https://pinage404.gitlab.io/git-gamble/contributing/index.html)

Feel free to [add package to your favorite package repository](https://pinage404.gitlab.io/git-gamble/contributing/add_to_package_repository.html)
