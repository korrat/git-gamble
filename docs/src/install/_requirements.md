<!-- markdownlint-disable-next-line MD041 -->
## Requirements

`git-gamble` doesn't repackage `git`, it uses the one installed on your system

You have to [install `git` manually](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Make sure it is available in your `$PATH`, you can check it with this command

```shell
git --help
```
