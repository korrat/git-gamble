# AppImage

[![AppImage available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=AppImage&prefix=v&logo=linux)](https://gitlab.com/pinage404/git-gamble/-/packages)

{{#include ../_requirements.md}}

## Install latest main version

1. Download the [latest main version](https://gitlab.com/pinage404/git-gamble/-/jobs/artifacts/main/raw/target%2Fgit-gamble-v-x86_64.AppImage?job=package%20AppImage)

   ```shell
   curl --location "https://gitlab.com/pinage404/git-gamble/-/jobs/artifacts/main/raw/target%2Fgit-gamble-v-x86_64.AppImage?job=package%20AppImage" --output git-gamble-x86_64.AppImage
   ```

1. Make it executable

   ```shell
   chmod +x git-gamble-x86_64.AppImage
   ```

1. Put it your `$PATH`

   ```shell
   # for example
   mkdir -p ~/.local/bin
   ln git-gamble-x86_64.AppImage ~/.local/bin/git-gamble
   export PATH+=":~/.local/bin"
   ```

## Install latest released version

1. Download the [latest released version](https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/generic/git-gamble-AppImage/2.7.0/git-gamble-v2.7.0-x86_64.AppImage)

   ```shell
   curl --location "https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/generic/git-gamble-AppImage/2.7.0/git-gamble-v2.7.0-x86_64.AppImage" --output git-gamble-v2.7.0-x86_64.AppImage
   ```

1. Make it executable

   ```shell
   chmod +x git-gamble-v2.7.0-x86_64.AppImage
   ```

1. Put it your `$PATH`

   ```shell
   # for example
   mkdir -p ~/.local/bin
   ln git-gamble-v2.7.0-x86_64.AppImage ~/.local/bin/git-gamble
   export PATH+=":~/.local/bin"
   ```

## Update

There is no update mechanism

## Uninstall

Just remove the binary from your `$PATH`
