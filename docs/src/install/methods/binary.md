# Download the binary

[![Release](https://img.shields.io/gitlab/v/release/pinage404/git-gamble?logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/releases)

{{#include ../_requirements.md}}

## Install

Only for Linux and Windows `x86_64`

1. Download the binary on the [release page](https://gitlab.com/pinage404/git-gamble/-/releases/version%2F2.7.0)
1. Put it in your `$PATH`

## Update

There is no update mechanism

## Uninstall

Just remove the binary from your `$PATH`
