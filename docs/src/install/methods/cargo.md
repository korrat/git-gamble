# Cargo

[![Crate available on Crates.io](https://img.shields.io/crates/d/git-gamble.svg?logo=rust)](https://crates.io/crates/git-gamble)

{{#include ../_requirements.md}}

## Install

[Install Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)

```shell
cargo install git-gamble
```

Add `~/.cargo/bin` to your `$PATH`

Fish:

```fish
set --export --append PATH ~/.cargo/bin
```

Bash / ZSH:

```bash
export PATH=$PATH:~/.cargo/bin
```

## Update

```shell
cargo update --package git-gamble
```

## Uninstall

```shell
cargo uninstall git-gamble
```
