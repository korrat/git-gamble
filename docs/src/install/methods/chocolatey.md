# Windows / Chocolatey

[![Chocolatey available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=Chocolatey&prefix=v&logo=chocolatey)](https://gitlab.com/pinage404/git-gamble/-/packages)

{{#include ../_requirements.md}}

## Install

[Install Chocolatey](https://chocolatey.org/)

1. Go to [the package registry page](https://gitlab.com/pinage404/git-gamble/-/packages)
1. Go to the latest version of `git-gamble.portable`
1. Download the latest version `git-gamble.portable.2.7.0.nupkg`
1. Install package

   Follow [GitLab's documentation](https://docs.gitlab.com/ee/user/packages/nuget_repository/index.html) and [Chocolatey's documentation](https://docs.chocolatey.org/en-us/choco/commands/install)

   ```shell
   choco install ./git-gamble.portable.2.7.0.nupkg
   ```

{{#include ../_please_improve_install.md}}

## Update

```shell
choco upgrade git-gamble
```

## Uninstall

```shell
choco uninstall git-gamble
```
