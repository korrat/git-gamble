# Debian

[![Debian available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=Debian&prefix=v&logo=debian)](https://gitlab.com/pinage404/git-gamble/-/packages)

{{#include ../_requirements.md}}

## Install

1. Go to [the package registry page](https://gitlab.com/pinage404/git-gamble/-/packages)
1. Go to the latest version of `git-gamble-debian`
1. Download the latest version `git-gamble_2.7.0_amd64.deb`
1. Install package

   As **root**

   ```shell
   dpkg --install git-gamble*.deb
   ```

This is not really convenient but a better way will come when [GitLab Debian Package Manager MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/5835) will be available

{{#include ../_please_improve_install.md}}

## Update

There is no update mechanism

## Uninstall

As **root**

```shell
dpkg --remove git-gamble
```
