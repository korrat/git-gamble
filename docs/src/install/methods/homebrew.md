# Mac OS X / Homebrew

[![Homebrew available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=Homebrew&prefix=v&logo=homebrew)](https://gitlab.com/pinage404/git-gamble/-/packages)

[Install Homebrew](https://brew.sh/)

```shell
brew tap pinage404/git-gamble https://gitlab.com/pinage404/git-gamble.git
brew install --HEAD git-gamble
```

## Upgrade

`git-gamble` has not yet been packaged by Homebrew

To upgrade `git-gamble`, run this command

```shell
brew reinstall git-gamble
```

{{#include ../_please_improve_install.md}}

## Uninstall

```shell
brew uninstall git-gamble
brew untap pinage404/git-gamble
```
