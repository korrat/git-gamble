# Nix / NixOS

Install [Nix](https://github.com/DeterminateSystems/nix-installer)

Use without installing

```bash
export NIX_CONFIG="extra-experimental-features = flakes nix-command"
nix run gitlab:pinage404/git-gamble -- --help
```

To install it at

- project level with [`flake.nix`, see example](https://gitlab.com/pinage404/git-gamble/-/tree/main/packaging/nix/flake/)

  **TL;DR**

  ```bash
  export NIX_CONFIG="extra-experimental-features = flakes nix-command"
  nix flake init --template gitlab:pinage404/git-gamble
  ```

- project level with [`shell.nix`, see example](https://gitlab.com/pinage404/git-gamble/-/tree/main/packaging/nix/legacy/)
- user level with [Home Manager, see example](https://gitlab.com/pinage404/git-gamble/-/tree/main/packaging/nix/home-manager/)

## DirEnv

To automate the setup of the environment it's recommanded to install [DirEnv](https://direnv.net/)

Add in `.envrc`

```direnv
use nix
```

Then run this command

```shell
direnv allow
```

## Cachix

If you want to avoid rebuilding from scratch, you can use [Cachix](https://app.cachix.org/cache/git-gamble#pull)

```shell
cachix use git-gamble
```

## Update

```bash
export NIX_CONFIG="extra-experimental-features = flakes nix-command"
nix flake update
```
