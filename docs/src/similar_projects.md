# Similar projects

- [`tcrdd`](https://github.com/FaustXVI/tcrdd) by Xavier Detant
  - He introduced me to :
    - [TDD](theory.md#tdd)
    - Rust
    - [TCR](theory.md#tcr)
    - [TCRDD](theory.md#tcrdd)
      - AFAIK, he invented TCRDD
- [TCR](https://github.com/murex/TCR) by Murex

## Reinvent the wheel

> Why reinvent the wheel?
>
> The [script](https://github.com/FaustXVI/tcrdd) of Xavier Detant already works well

Because i would like to learn Rust `¯\_(ツ)_/¯`
