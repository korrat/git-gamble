# History of TCRDD

```mermaid
timeline
    title History of TCRDD
    2018
        : TCR                  `test && commit || revert`       Article                  by Kent Beck 2018-09-28
        : TCRDD          _TDD is dead, long live TCR?_ Article                  by Xavier Detant 2018-12-03
    2019
        : Xavier's TCRDD First commit 2019-08-26
        : git-gamble        First commit 2019-12-07
    2021 : Murex's TCR  First commit 2021-06-16
```

- [test && commit || revert](https://medium.com/@kentbeck_7670/test-commit-revert-870bbd756864)
- [TDD is dead, long live TCR?](https://blog.zenika.com/2018/12/03/tdd-est-mort-longue-vie-tcr/)
- [Xavier's TCRDD first commit](https://github.com/FaustXVI/tcrdd/commit/1842b0c342212c09c28f3a650cde8b7ac5842f85)
- [git-gamble's first commit](https://gitlab.com/pinage404/git-gamble/-/commit/077c3e01064ee7923917a919ccb0cb581653f4c5)
- [Murex's TCR first commit](https://github.com/murex/TCR/commit/a823c098187455bade90ee44874d2b41c7ef96d9)
