# Theory

Alternatively : you can watch [the slides about the theory](https://pinage404.gitlab.io/git-gamble/slides_theory)

## TDD

{{#include ./theory/tdd/_description.md}}

### Short version

{{#include ./theory/tdd/graph/_all_in_one.md}}

### Long version

{{#include ./theory/tdd/graph/_red.md}}

{{#include ./theory/tdd/graph/_green.md}}

{{#include ./theory/tdd/graph/_refactor.md}}

## TCR

{{#include ./theory/tcr/_description.md}}

{{#include ./theory/tcr/graph/_simplified.md}}

{{#include ./theory/tcr/graph/_overview_tdd.md}}

{{#include ./theory/tcr/_but.md}}

### Detailed view

{{#include ./theory/tcr/graph/_detailed_view.md}}

## TCRDD

{{#include ./theory/tcrdd/_description.md}}

{{#include ./theory/tcrdd/graph/_all_in_one.md}}

{{#include ./theory/tcrdd/graph/_red.md}}

{{#include ./theory/tcrdd/graph/_green.md}}

{{#include ./theory/tcrdd/graph/_refactor.md}}

`git-gamble` {{#include ./_logo_mini_front.html}} is a tool that helps to use the TCRDD method
