```mermaid
flowchart TB
    subgraph green[Green]
        direction TB

        start([Start])
        ==> change_code[Change some code]
        ==> run_tests{{"Run tests\n<code>test && commit || revert</code>"}}
        ==>|Pass| commit(Commit)
        ==> finish([Finish])

        commit
        -->|Another things to change ?| change_code

        run_tests
        -.->|Fail| revert(Revert)
        -.->|Try something else| change_code
    end

    classDef green_phase font-weight:bold,color:black,fill:#1cba1c;
    class green green_phase
```
