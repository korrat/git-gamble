**T**_CRDD_ = **T**_CR_ + **T**_DD_

<v-click>

TCRDD **blends** the constraints of the two **methods**
to **benefit** from their **advantages**

</v-click>

<v-click>
Therefore,
TCRDD makes sure we <strong>develop</strong> the <strong>right</strong> thing,
step by step,
</v-click>

<v-click>
and we are <strong>encouraged</strong> to do so by <strong>baby steps</strong>,
reducing the waste when we are wrong
</v-click>
