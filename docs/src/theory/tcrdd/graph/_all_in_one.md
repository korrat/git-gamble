```mermaid
flowchart TB
    red{{Write only 1 failing test}}
    green{{Make all tests pass}}
    refactor{{Refactor}}

    red ==>|Fail| red_commit(Commit) ==> green
    green ==>|Pass| green_commit(Commit) ==> refactor
    refactor ==>|Pass| refactor_commit(Commit)

    red -->|Pass| red_revert(Revert) --> red
    green -->|Fail| green_revert(Revert) --> green
    refactor -->|Fail| refactor_revert(Revert) --> refactor

    classDef red_phase font-weight:bold,color:black,fill:coral;
    class red red_phase

    classDef green_phase font-weight:bold,color:black,fill:#1cba1c;
    class green green_phase

    classDef refactor_phase font-weight:bold,color:black,fill:#668cff;
    class refactor refactor_phase
```
