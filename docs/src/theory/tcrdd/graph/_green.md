```mermaid
flowchart TB
    subgraph green[Green]
        direction LR

        fix_layout(( )) ==> write_the_minimum_code

        write_the_minimum_code[Write the minimum code]
        ==> gamble[/Gamble that the tests pass\n<code>git gamble --green</code>/]
        ==> run_tests{{Actually run tests}}
        ==>|Pass| commit(Commit)

        run_tests
        -->|Fail| revert(Revert)
        -->|Try something else| write_the_minimum_code
    end

    red([Red]) ==> fix_layout

    commit ==> refactor([Refactor])

    classDef red_phase font-weight:bold,color:black,fill:coral;
    class red red_phase

    classDef green_phase font-weight:bold,color:black,fill:#1cba1c;
    class green green_phase

    classDef refactor_phase font-weight:bold,color:black,fill:#668cff;
    class refactor refactor_phase

    classDef fix_layout stroke:white,fill:transparent;
    class fix_layout fix_layout
```
