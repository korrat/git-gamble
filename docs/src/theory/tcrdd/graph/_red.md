```mermaid
flowchart TB
    subgraph red[Red]
        direction LR

        fix_layout(( )) ==> write_a_test

        write_a_test[Write only 1 test]
        ==> gamble[/Gamble that the test fail\n<code>git gamble --red</code>/]
        ==> run_tests{{Actually run tests}}
        ==>|Fail| commit(Commit)

        run_tests
        -->|Pass| revert(Revert)
        -->|Write another test| write_a_test
    end

    start([Start]) ==> fix_layout

    commit ==> green([Green])

    classDef red_phase font-weight:bold,color:black,fill:coral;
    class red red_phase

    classDef green_phase font-weight:bold,color:black,fill:#1cba1c;
    class green green_phase

    classDef refactor_phase font-weight:bold,color:black,fill:#668cff;
    class refactor refactor_phase

    classDef fix_layout stroke:white,fill:transparent;
    class fix_layout fix_layout
```
