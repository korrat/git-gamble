_[**TDD** (Test Driven Development)][tdd] is cool!_

<v-click>

It makes sure we **develop** the **right** thing, step by step

</v-click>

[tdd]: https://en.wikipedia.org/wiki/Test-driven_development
