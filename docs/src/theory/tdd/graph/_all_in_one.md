```mermaid
flowchart TB
    start([Start])
    ==> red([Write only 1 failing test])
    ==> green([Make all tests pass])
    ==> refactor([Refactor])
    ==> finish([Finish])

    refactor -.->|Incomplete ?| red

    classDef red_phase font-weight:bold,color:black,fill:coral;
    class red red_phase

    classDef green_phase font-weight:bold,color:black,fill:#1cba1c;
    class green green_phase

    classDef refactor_phase font-weight:bold,color:black,fill:#668cff;
    class refactor refactor_phase
```
