```mermaid
flowchart TB
    subgraph green[Green]
        direction LR

        fix_layout(( )) ==> write_the_minimum_code

        write_the_minimum_code[Write the minimum code\nto make all tests pass]
        ==> run_tests{{Run tests}}

        run_tests
        -.->|Fail\nTry something else| write_the_minimum_code
    end

    red([Red]) === fix_layout

    run_tests ==>|Pass| refactor([Refactor])

    classDef red_phase font-weight:bold,color:black,fill:coral;
    class red red_phase

    classDef green_phase font-weight:bold,color:black,fill:#1cba1c;
    class green green_phase

    classDef refactor_phase font-weight:bold,color:black,fill:#668cff;
    class refactor refactor_phase

    classDef fix_layout stroke:white,fill:transparent;
    class fix_layout fix_layout
```
