# Usage

{{#include ../_logo.html}}

## How to use ?

To see all available flags and options

```shell
git-gamble --help # dash between `git` and `gamble` is only needed for --help
```

There are two ways to run your tests using git gamble :

- by [setting the test command in an environment variable](#environment-variable)
- by [repeating the test command](#repeating-the-test-command)

In both cases,
the test command must exit with a 0 status when there are 0 failed tests,
anything else is considered as a failure

### Environment variable

Setting an environment variable and run only the `git command` command

1. Start by setting a environment variable with the test command :

   The example below is for running your tests for a Rust project

   ```shell
   export GAMBLE_TEST_COMMAND="cargo test"
   ```

1. Write a failing test in your codebase, then :

   ```shell
   git gamble --red
   # or
   git gamble --fail
   ```

1. Write the minimum code to make tests pass, then :

   ```shell
   git gamble --green
   # or
   git gamble --pass
   ```

1. Refactor your code, then :

   ```shell
   git gamble --refactor
   # or
   git gamble --pass
   ```

### Repeating the test command

Typing the `git gamble` command with your test command _repetitively_

1. Write a failing test in your codebase, then :

   ```shell
   git gamble --red -- $YOUR_TEST_COMMAND
   ```

1. Write the minimum code to make tests pass, then :

   ```shell
   git gamble --green -- $YOUR_TEST_COMMAND
   ```

1. Refactor your code, then :

   ```shell
   git gamble --refactor -- $YOUR_TEST_COMMAND
   ```

### Demo

For more detailed example, this the [demo](demo/index.html)

[![asciicast](https://asciinema.org/a/496959.svg)](demo/index.html)

## When to use it ?

- You want to :
  - learn nor practice [TDD](../theory.md#tdd)
  - learn nor practice [TCR without the drawbacks](../theory.md#tcr)
  - practice a new language
  - be driven by a strong methodology
  - be carried along by the strong methodology
  - practice breaking down your steps to achieve baby steps, using, for example :
    - [Refactoring Catalog](https://refactoring.com/catalog/) of [Martin Fowler](https://martinfowler.com/)
    - [TPP : Transformation Priority Premise](https://en.wikipedia.org/wiki/Transformation_Priority_Premise)
- You follow the [Test F.I.R.S.T. Principles](https://dev.to/mundim/writing-your-f-i-r-s-t-unit-tests-1iop)

## When NOT to use it ?

- You don't really want to try this methodology
  - You will eventually cheat and lose the benefits while keeping some of the drawbacks
- You don't know what you want to do
- Your tests :
  - ran very slowly
  - are flaky
  - are not self-validating
- You want to use double-loop TDD
  - unless you don't run tests on bigger loops when you run smaller loops
