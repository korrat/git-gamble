# Git Aliases

[Some examples](https://gitlab.com/pinage404/dotfiles/-/blob/56c2faadeeb1e531229db77109ad82d84ecd714d/dotfiles/config/git/git-gamble.gitconfig) of [git alias](https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases)

`~/.gitconfig` or `.git/config`

```toml
[alias]
    # git-gamble aliases
    fail = gamble --fail
    pass = gamble --pass
    faile = gamble --fail --edit
    passe = gamble --pass --edit

    # git-gamble TDD's aliases
    red = gamble --red
    green = gamble --green
    refactor = gamble --refactor
    rede = gamble --red --edit
```

If you use [Fish Shell](https://fishshell.com/), it works very well with the [Oh My Fish](https://www.github.com/oh-my-fish/oh-my-fish) plugin [`enlarge_your_git_alias`](https://gitlab.com/pinage404/omf_pkg_enlarge_your_git_alias)
