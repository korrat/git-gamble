# Demo

<!-- markdownlint-disable MD033 -->

`git-gamble`'s demo to develop using TCRDD by doing baby steps

on a simple program

`git-gamble` works with all languages and tools and editors

for this example, i use `python` with `pytest` and `nano`

```sh
export GAMBLE_TEST_COMMAND='pytest --quiet'
```

note : for a simpler demo, test code and production code are in the same file

Alternatively : you can also watch [the slides about the demo](https://pinage404.gitlab.io/git-gamble/slides_demo)

---

## First TDD loop ➰

> Write a program that says `Hello world`

### First, 🔴 Red phase : write a test that fails for the good reason

<script src="https://asciinema.org/a/496949.js" id="asciicast-496949" async></script>

<details>
<summary>Alternative text</summary>

```sh
nano test_hello.py
```

```python
def hello():
    pass

def test_say_hello_world():
    assert hello() == 'Hello world'
```

Then, gamble that the tests fail

```sh
git gamble --red
```

✔️ Committed

</details>

### Second, 🟢 Green phase : write the minimum code to pass the tests

Let's fake it

<script src="https://asciinema.org/a/496951.js" id="asciicast-496951" async></script>

<details>
<summary>Alternative text</summary>

```sh
nano test_hello.py
```

```python
def hello():
    return 'Hello word'
```

Then, gamble that the tests pass

```sh
git gamble --green
```

❌ Reverted

</details>

Oh no !

I made a typo

```diff
 def hello():
-    return 'Hello word'
+    return 'Hello world'
```

Try again

Let's fake it **without typo**

<script src="https://asciinema.org/a/496952.js" id="asciicast-496952" async></script>

<details>
<summary>Alternative text</summary>

```sh
nano test_hello.py
```

```python
def hello():
    return 'Hello world'
```

Gamble again that the tests pass

```sh
git gamble --green
```

✔️ Committed

</details>

Yeah !

### Third, 🔀 Refactor phase : Nothing to refactor yet

---

Then 🔁 Repeat ➰

## Second TDD loop ➿

> Write a program that says `Hello` to the given name when a name is given

### 🔴 Red phase

<script src="https://asciinema.org/a/496957.js" id="asciicast-496957" async></script>

<details>
<summary>Alternative text</summary>

```sh
nano test_hello.py
```

```python
def test_say_hello_name_given_a_name():
    assert hello('Jon') == 'Hello Jon'
```

```sh
git gamble --red
```

✔️ Committed

</details>

### 🟢 Green phase

Add a simple condition to handle both cases

<script src="https://asciinema.org/a/496959.js" id="asciicast-496959" async></script>

<details>
<summary>Alternative text</summary>

```sh
nano test_hello.py
```

```python
def hello(arg=None):
    if arg:
        return f'Hello {arg}'
    return 'Hello world'
```

```sh
git gamble --green
```

✔️ Committed

</details>

### 🔀 Refactor loop ➰

#### 🔀 Refactor phase

It can be simplified

<script src="https://asciinema.org/a/496961.js" id="asciicast-496961" async></script>

<details>
<summary>Alternative text</summary>

```sh
nano test_hello.py
```

```python
def hello(arg='world'):
    return f'Hello {arg}'
```

```sh
git gamble --refactor
```

✔️ Committed

</details>

#### Still 🔀 Refactor phase : i have something else to refactor ➿

Better naming

<script src="https://asciinema.org/a/496963.js" id="asciicast-496963" async></script>

<details>
<summary>Alternative text</summary>

```sh
nano test_hello.py
```

```python
def hello(name='world'):
    return f'Hello {name}'
```

```sh
git gamble --refactor
```

✔️ Committed

</details>

---

And so on... ➿

🔁 Repeat until you have tested all the rules, are satisfied and enougth confident
