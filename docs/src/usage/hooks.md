# Hooks

`git-gamble` provides his own custom [hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) :

- `pre-gamble <GAMBLED>`
  - `pre-gamble` hook is executed with one argument `<GAMBLED>`
- `post-gamble <GAMBLED> <ACTUAL>`
  - `post-gamble` hook is executed with two arguments `<GAMBLED>` and `<ACTUAL>`

Where :

- `<GAMBLED>` is `pass` or `fail`
- `<ACTUAL>` is `pass` or `fail`

Custom hooks of `git-gamble` are like any other client-side [hooks](https://git-scm.com/docs/git-hook) :

- a hook is a file
- a hook must be in the `$GIT_DIR/hooks/` folder
  - or in the folder configured by `git config core.hooksPath`
- a hook must be executable (`chmod +x .git/hooks/*-gamble`)
- will not be executed if any of these options are used:
  - `--no-verify`
  - `--dry-run`

Check [the `hooks`' folder](https://gitlab.com/pinage404/git-gamble/-/blob/main/.config/git/hooks/) for examples of use

- [`post-gamble.real_time_collaboration.sample.sh`](https://gitlab.com/pinage404/git-gamble/-/blob/main/.config/git/hooks/post-gamble.real_time_collaboration.sample.sh) is specially adapted to _near_ **real-time collaboration**
- [`post-gamble.assistant.sample.sh`](https://gitlab.com/pinage404/git-gamble/-/blob/main/.config/git/hooks/post-gamble.assistant.sample.sh) is a simple assistant that displays tips based on the result of the gamble
- If you are using [`git-time-keeper`](../wip/git-time-keeper.md)
  - [`pre-gamble.time-keeper.sample.sh`](https://gitlab.com/pinage404/git-gamble/-/blob/main/.config/git/hooks/pre-gamble.time-keeper.sample.sh)
  - [`post-gamble.time-keeper.sample.sh`](https://gitlab.com/pinage404/git-gamble/-/blob/main/.config/git/hooks/post-gamble.time-keeper.sample.sh)

The following diagram shows when custom hooks are executed in relation to normal git hooks

```mermaid
flowchart LR
    subgraph git-gamble's hooks' lifecyle
        direction TB
        git-gamble([git-gamble\n--pass OR --fail])
        --> pre-gamble[pre-gamble\npass OR fail]:::gitGambleHookStyle
        --> GAMBLE_TEST_COMMAND([exec $GAMBLE_TEST_COMMAND]):::gitGambleInternalStyle
        --> gamble{Result ?}:::gitGambleInternalStyle

        gamble -->|Success| Success
        subgraph Success
            direction TB

            git_add([git add --all]):::gitGambleInternalStyle
            --> git_commit([git commit]):::gitGambleInternalStyle
            --> pre-commit:::gitHookStyle
            --> prepare-commit-msg[prepare-commit-msg\n$GIT_DIR/COMMIT_EDITMSG\nmessage]:::gitHookStyle
            --> commit-msg[commit-msg\n$GIT_DIR/COMMIT_EDITMSG]:::gitHookStyle
            --> post-commit:::gitHookStyle
            post-commit --> rewritten?
            rewritten?{{"Last commit rewritten ?\nWhen gambling fail\nafter another gamble fail"}}:::gitGambleInternalStyle
            rewritten? -->|Yes| post-rewrite[post-rewrite\namend]:::gitHookStyle --> post-gamble-success
            rewritten? -->|No| post-gamble-success
            post-gamble-success[post-gamble\npass OR fail\nsuccess]:::gitGambleHookStyle
        end

        gamble -->|Error| Error
        subgraph Error
            direction TB

            git_reset([git reset --hard]):::gitGambleInternalStyle
            --> post-gamble-error[post-gamble\npass OR fail\nerror]:::gitGambleHookStyle
        end
    end

    subgraph Legend
        direction TB

        subgraph Legend_[" "]
            direction LR

            command([Command executed by user])
            git-gamble_command([Command executed by git-gamble]):::gitGambleInternalStyle
            condition{Condition ?}:::gitGambleInternalStyle
        end

        subgraph Hooks
            direction LR

            hook[git's hook]:::gitHookStyle
            hook_with_argument[git's hook\nfirst argument\nsecond argument]:::gitHookStyle
            git-gamble_hook_with_argument[git-gamble's hook\nfirst argument\nsecond argument]:::gitGambleHookStyle
        end
    end

    classDef gitHookStyle fill:#f05133,color:black,stroke:black;
    classDef gitGambleHookStyle fill:#5a3730,color:white,stroke:white;
    classDef gitGambleInternalStyle fill:#411d16,color:white,stroke:white;
```
