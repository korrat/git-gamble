# VSCode

If you want to run `git-gamble` from VSCode

Add this in your [`.vscode/tasks.json`](https://code.visualstudio.com/docs/editor/tasks#_custom-tasks)

```json
{{#include ../../../.vscode/tasks.json}}
```
