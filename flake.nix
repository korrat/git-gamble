{
  # broken on Fish
  # as a workaround, use `nix develop` the first time
  # https://github.com/direnv/direnv/issues/1022
  nixConfig.extra-substituters = [ "https://git-gamble.cachix.org" ];
  nixConfig.extra-trusted-public-keys = [ "git-gamble.cachix.org-1:afbVJAcYMKSs3//uXw3HFdyKLV66/KvI4sjehkdMM/I=" ];

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    flake-utils.url = "github:numtide/flake-utils";

    # used only for retro compatibility with `nix-shell`
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    masklint = {
      url = "github:brumhard/masklint/latest";
      inputs.utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    , flake-compat
    , rust-overlay
    , crane
    , ...
    }@inputs:
    # uncomment to debug with `traceVal someValue` `traceSeq someValue` `traceValSeq someValue` `traceValSeqN 2 someValue`
    # with import ./nix/debug.nix { inherit nixpkgs; };
    (flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          rust-overlay.overlays.default
        ];
      };

      rust-toolchain = (pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml);

      craneLib = (crane.mkLib pkgs).overrideToolchain rust-toolchain;

      src = craneLib.cleanCargoSource ./.;

      rust-dependencies = craneLib.buildDepsOnly {
        inherit src;
      };

      rust-package-binary = craneLib.buildPackage {
        pname = "git-gamble-binary";
        inherit src;
        cargoArtifacts = rust-dependencies;

        doCheck = false;
      };

      package = craneLib.buildPackage {
        src = pkgs.lib.sources.cleanSource ./.;
        cargoArtifacts = rust-package-binary;

        preCheck = ''
          patchShebangs tests/editor/fake_editor.sh
        '';

        buildInputs = [
          pkgs.git
        ];

        nativeBuildInputs = [
          pkgs.installShellFiles
        ];
        postInstall = ''
          export PATH="$PATH:target/release/"

          sh ./script/generate_completion.sh target/release/shell_completions/
          installShellCompletion --bash target/release/shell_completions/git-gamble.bash
          installShellCompletion --fish target/release/shell_completions/git-gamble.fish
          installShellCompletion --zsh target/release/shell_completions/_git-gamble

          sh ./script/usage.sh > git-gamble.1
          installManPage git-gamble.1
        '';
      };

      app = flake-utils.lib.mkApp {
        drv = package;
      };
    in
    {
      # run with `nix flake check`
      checks = {
        inherit package;
      };

      # run with `nix develop`
      devShells.default = pkgs.mkShellNoCC {
        packages = [
          rust-toolchain
          (import ./nix/shell_packages.nix {
            inherit
              pkgs
              inputs
              system
              ;
          })
        ];
      };

      devShells.slides = pkgs.mkShellNoCC {
        packages = [
          pkgs.nodejs
          pkgs.mask
        ];
      };

      # run with `nix shell`
      packages.git-gamble-deps = rust-dependencies;
      packages.git-gamble-binary = rust-package-binary;
      packages.git-gamble = package;
      packages.default = package;

      # run with `nix run`
      apps.git-gamble = app;
      apps.default = app;

      # run with `nix fmt`
      formatter = pkgs.nixpkgs-fmt;
    }
    ))
    //
    (
      let
        overlay = final: prev: {
          git-gamble = self.packages.${prev.stdenv.hostPlatform.system}.git-gamble;
        };
      in
      {
        overlays.git-gamble = overlay;
        overlays.default = overlay;

        # run with `nix flake init --template gitlab:pinage404/git-gamble`
        templates.default = {
          description = "A basic flake with git-gamble";
          path = ./packaging/nix/flake;
        };
      }
    )
  ;
}
