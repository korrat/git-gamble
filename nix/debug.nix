# add the following line to debug with `traceVal someValue` `traceSeq someValue` `traceValSeq someValue` `traceValSeqN 2 someValue`
# with import ./nix/debug.nix {};
# or
# with import ./nix/debug.nix { inherit nixpkgs; };

let
  defaultNixpkgs = builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/8588b14a397e045692d0a87192810b6dddf53003.tar.gz";
    sha256 = "sha256:15srsgbhgn27wa4kz4x0gfqbsdnwig0h0y8gj2h4nnw92nrxpvnm";
  };
in
{ nixpkgs ? defaultNixpkgs }:
let
  pkgs = builtins.import nixpkgs {
    system = "x86_64-linux";
  };
in
pkgs.lib.debug
