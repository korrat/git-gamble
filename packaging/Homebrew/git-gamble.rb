class GitGamble < Formula
  desc "git-gamble is a tool that blends TDD + TCR to make sure to develop the right thing 😌, baby step by baby step 👶🦶"
  homepage "https://pinage404.gitlab.io/git-gamble/"
  head "https://gitlab.com/pinage404/git-gamble.git"

  depends_on "rust" => :build

  def install
    system "cargo", "install", "--locked", "--root", prefix, "git-gamble"

    generate_completions_from_executable(bin/"git-gamble", "generate-shell-completions")
  end

  test do
    system "git", "init"
    system "git", "config", "user.name", "Git Gamble"
    system "git", "config", "user.email", "git@gamble"

    system "sh", "-c", "echo 'failing' > 'some_file'"
    system "#{bin}/git-gamble", "--fail", "--", "false"
    assert_equal "failing", shell_output("cat some_file").strip

    system "sh", "-c", "echo 'should pass but still failing' > 'some_file'"
    system "#{bin}/git-gamble", "--pass", "--", "false"
    assert_equal "failing", shell_output("cat some_file").strip

    system "sh", "-c", "echo 'passing' > 'some_file'"
    system "#{bin}/git-gamble", "--pass", "--", "true"
    assert_equal "passing", shell_output("cat some_file").strip
  end
end
