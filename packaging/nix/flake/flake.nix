{
  description = "A basic flake with git-gamble";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.git-gamble.url = "gitlab:pinage404/git-gamble";
  inputs.git-gamble.inputs.nixpkgs.follows = "nixpkgs";
  inputs.git-gamble.inputs.flake-utils.follows = "flake-utils";

  outputs = { self, nixpkgs, flake-utils, git-gamble }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      {
        devShell = pkgs.mkShellNoCC {
          packages = [
            git-gamble.packages."${system}".git-gamble
          ];

          shellHook = ''
            export GAMBLE_TEST_COMMAND="true" # replace `true` with the command to run your tests
          '';
        };
      });

  # uncomment the following to
  # avoid build by using cache from Cachix
  # note: this is broken on Fish
  # as a workaround, use `nix develop` the first time
  # https://github.com/direnv/direnv/issues/1022
  # nixConfig.extra-substituters = [
  #   "https://git-gamble.cachix.org"
  # ];
  # nixConfig.extra-trusted-public-keys = [
  #   "git-gamble.cachix.org-1:afbVJAcYMKSs3//uXw3HFdyKLV66/KvI4sjehkdMM/I="
  # ];
}
