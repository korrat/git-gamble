{ version ? "2.7.0"
, sha256 ? "1111111111111111111111111111111111111111111111111111"
, cargoSha256 ? "0000000000000000000000000000000000000000000000000000"

, rustPlatform
, fetchFromGitLab
, git
, lib
, installShellFiles
}:

rustPlatform.buildRustPackage rec {
  pname = "git-gamble";

  inherit version cargoSha256;

  src = fetchFromGitLab {
    owner = "pinage404";
    repo = pname;
    rev = "version/${version}";
    inherit sha256;
  };

  # git is needed at runtime
  # don't know which of these "propagated" values should be used, both of them works:
  # * depsBuildBuildPropagated
  # * propagatedNativeBuildInputs
  # * depsBuildTargetPropagated
  # * depsHostHostPropagated
  # * propagatedBuildInputs
  # * depsTargetTargetPropagated
  depsTargetTargetPropagated = [ git ];

  doCheck = false;

  meta = {
    description = "git-gamble is a tool that blends TDD + TCR to make sure to develop the right thing 😌, baby step by baby step 👶🦶";
    homepage = "https://pinage404.gitlab.io/git-gamble/";
    changelog = "https://gitlab.com/pinage404/git-gamble/-/blob/main/CHANGELOG.md";
    license = lib.licenses.isc;
  };
}
