{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";

    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";

    home-manager.url = "github:nix-community/home-manager/release-22.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.inputs.utils.follows = "flake-utils";

    git-gamble.url = "gitlab:pinage404/git-gamble";
    git-gamble.inputs.nixpkgs.follows = "nixpkgs";
    git-gamble.inputs.flake-utils.follows = "flake-utils";
  };

  outputs = { self, flake-utils, nixpkgs, home-manager, git-gamble }: {
    homeConfigurations = {
      jdoe = home-manager.lib.homeManagerConfiguration rec{
        pkgs = import nixpkgs {
          system = "x86_64-linux";
          overlays = [
            git-gamble.overlays.default
          ];
        };
        modules = [ ./home.nix ];
      };
    };
  };
}
