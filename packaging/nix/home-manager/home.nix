{ pkgs, ... }:

{
  home.packages = [
    pkgs.git-gamble
  ];

  home.username = "jdoe";
  home.homeDirectory = "/home/jdoe";
  home.stateVersion = "22.11";
}
