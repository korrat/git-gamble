{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShellNoCC {
  packages = [
    pkgs.home-manager
  ];
}
