# Legacy / project level

Install [Nix](https://nixos.org/)

Install [DirEnv](https://direnv.net/)

Copy [`shell.nix`](./shell.nix)
and [`.envrc`](./.envrc)
in your project

Then run this command

```shell
direnv allow
```

or

```shell
nix-shell
```

You will have to run the command several times to change the `sha256` and `cargoSha256` by the one given by Nix
