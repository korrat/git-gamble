#!/usr/bin/env sh

REAL_PATH=$(realpath "$0")
DIRECTORY_PATH=$(dirname "${REAL_PATH}")

. "${DIRECTORY_PATH}/../skip_lines_after_pattern.sh"

test_skip_lines_after_pattern() {
    PATTERN="---"
    EXPECTED_CONTENT="some expected content

${PATTERN}"

    ACTUAL_CONTENT=$(
        echo "${EXPECTED_CONTENT}

some ignored content" |
            skip_lines_after_pattern "${PATTERN}"
    )

    assertEquals "contents are different" "${EXPECTED_CONTENT}" "${ACTUAL_CONTENT}"
}

. shunit2
