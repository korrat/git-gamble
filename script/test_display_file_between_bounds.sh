#!/usr/bin/env sh

REAL_PATH=$(realpath "$0")
DIRECTORY_PATH=$(dirname "${REAL_PATH}")

. "${DIRECTORY_PATH}/display_file_between_bounds.sh"

test_display_file_between_bounds() {
    PATTERN_BEGIN="from this"
    PATTERN_END="to that"
    EXPECTED_CONTENT="${PATTERN_BEGIN}

some expected content

${PATTERN_END}"

    ACTUAL_CONTENT=$(
        echo "
some ignored content

${EXPECTED_CONTENT}

other ignored content
" |
            display_file_between_bounds "${PATTERN_BEGIN}" "${PATTERN_END}"
    )

    assertEquals "contents are different" "${EXPECTED_CONTENT}" "${ACTUAL_CONTENT}"
}

. shunit2
