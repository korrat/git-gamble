#!/usr/bin/env sh

set -o errexit
set -o nounset

file_exists() {
	FILE_PATH="$1"

	test -f "${FILE_PATH}" && echo "file exists" || echo "file doesn't exist"
}

assert_file_exists() {
	FILE_PATH="$1"

	RESULT=$(file_exists "${FILE_PATH}")

	assertEquals "file exists" "${RESULT}"
}

assert_file_does_not_exist() {
	FILE_PATH="$1"

	RESULT=$(file_exists "${FILE_PATH}")

	assertEquals "file doesn't exist" "${RESULT}"
}
