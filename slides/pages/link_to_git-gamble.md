---
layout: fact
---

[`git-gamble` <span class="logo mini"></span>](https://pinage404.gitlab.io/git-gamble/)
is a tool that helps to develop the right thing 😌,
baby step by baby step 👶🦶
