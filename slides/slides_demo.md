---
favicon: '/logo.svg'
info: >
  git-gamble is a tool that
  blends TDD + TCR to make sure to develop the right thing 😌,
  baby step by baby step 👶🦶
addons:
  - slidev-component-zoom
selectable: true
canvasWidth: 780
lineNumbers: true
---

# git-gamble's demo

---
layout: center
---

<nav><Toc /></nav>

---
layout: section
---

## First TDD loop ➰

---
layout: quote
---

> Write a program that says `Hello world`

---
layout: gamble
gamble: red
result: committed
---

> **First, <Red /> phase:**
>
> Write a test that fails for the good reason

::code::

<v-click>

```python {monaco} {editorOptions:{lineNumbers:'on',renderLineHighlight:'none'}}
def hello():
    pass

def test_say_hello_world():
    assert hello() == 'Hello world'
```

</v-click>

---
layout: gamble
gamble: green
result: reverted
---

> **Second, <Green /> phase:**
>
> Write the minimum code to pass the tests

Let's fake it

::code::

```python {monaco-diff} {editorOptions:{lineNumbers:'on',renderLineHighlight:'none'}}
def hello():
    pass

def test_say_hello_world():
    assert hello() == 'Hello world'
~~~
def hello():
    return 'Hello word'

def test_say_hello_world():
    assert hello() == 'Hello world'
```

---
layout: default
---

Oh no !

I made a typo

```python {monaco-diff} {editorOptions:{lineNumbers:'on',renderLineHighlight:'none'}}
def hello():
    return 'Hello word'
                     ^
~~~
def hello():
    return 'Hello world'
                     ^
```

---
layout: gamble
gamble: green
result: committed
---

Try again

Let's fake it **without typo**

::code::

```python {monaco-diff} {editorOptions:{lineNumbers:'on',renderLineHighlight:'none'}}
def hello():
    pass

def test_say_hello_world():
    assert hello() == 'Hello world'
~~~
def hello():
    return 'Hello world'

def test_say_hello_world():
    assert hello() == 'Hello world'
```

---
layout: gamble
gamble: refactor
---

> **Third, <Refactor /> phase:**
>
> Nothing to refactor yet

---
layout: center
---

Then 🔁 Repeat ➰

---
layout: section
---

## Second TDD loop ➿

---
layout: quote
---

> Write a program that says `Hello` to the given name when a name is given

---
layout: gamble
gamble: red
result: committed
---

Add a test

::code::

```python {monaco-diff} {editorOptions:{lineNumbers:'on',renderLineHighlight:'none'}}
def hello():
    return 'Hello world'

def test_say_hello_world():
    assert hello() == 'Hello world'
~~~
def hello():
    return 'Hello world'

def test_say_hello_world():
    assert hello() == 'Hello world'

def test_say_hello_name_given_a_name():
    assert hello('Jon') == 'Hello Jon'
```

---
layout: gamble
gamble: green
result: committed
---

Add a simple condition to handle both cases

::code::

```python {monaco-diff} {editorOptions:{lineNumbers:'on',renderLineHighlight:'none'}}
def hello():
    return 'Hello world'

def test_say_hello_world():
    assert hello() == 'Hello world'

def test_say_hello_name_given_a_name():
    assert hello('Jon') == 'Hello Jon'
~~~
def hello(arg=None):
    if arg:
        return f'Hello {arg}'
    return 'Hello world'

def test_say_hello_world():
    assert hello() == 'Hello world'

def test_say_hello_name_given_a_name():
    assert hello('Jon') == 'Hello Jon'
```

---
layout: gamble
gamble: refactor
result: committed
---

> **<Refactor /> phase:**
>
> Rewrite the code without changing the behavior

It can be simplified

::code::

```python {monaco-diff} {editorOptions:{lineNumbers:'on',renderLineHighlight:'none'}}
def hello(arg=None):
    if arg:
        return f'Hello {arg}'
    return 'Hello world'

def test_say_hello_world():
    assert hello() == 'Hello world'

def test_say_hello_name_given_a_name():
    assert hello('Jon') == 'Hello Jon'
~~~
def hello(arg='world'):
    return f'Hello {arg}'

def test_say_hello_world():
    assert hello() == 'Hello world'

def test_say_hello_name_given_a_name():
    assert hello('Jon') == 'Hello Jon'
```

---
layout: gamble
gamble: refactor
result: committed
---

I have something else to refactor ➿

Better naming

::code::

```python {monaco-diff} {editorOptions:{lineNumbers:'on',renderLineHighlight:'none'}}
def hello(arg='world'):
    return f'Hello {arg}'

def test_say_hello_world():
    assert hello() == 'Hello world'

def test_say_hello_name_given_a_name():
    assert hello('Jon') == 'Hello Jon'
~~~
def hello(name='world'):
    return f'Hello {name}'

def test_say_hello_world():
    assert hello() == 'Hello world'

def test_say_hello_name_given_a_name():
    assert hello('Jon') == 'Hello Jon'
```

---

And so on... ➿

Repeat 🔁 until you have tested all the rules, are satisfied and enougth confident

---
src: ./pages/link_to_git-gamble.md
---

---
src: ./pages/link_to_slides_why.md
---

---
src: ./pages/link_to_slides_theory.md
---

---
src: ./pages/questions.html
---
