use clap::Parser;
use clap_complete::generate;
use clap_complete::Shell;
use std::io;
use std::process::exit;
use yansi::Paint;

use git_gamble::git_gamble::cli::Cli;
use git_gamble::git_gamble::cli::OptionalSubcommands;

use git_gamble::git_gamble::repository::Repository;

use git_gamble::git_gamble::command_choice::tcr;
use git_gamble::git_gamble::command_choice::trc;
use git_gamble::git_gamble::command_choice::RepositoryCommandChoice;

use git_gamble::git_gamble::generate_shell_completions::generate_shell_completions;

fn main() {
	#[cfg(feature = "with_log")]
	pretty_env_logger::init();

	human_panic::setup_panic!();

	let opt = Cli::parse();
	log::info!("{:#?}", opt);

	if let Some(subcommand_opt) = opt.optional_subcommands {
		match subcommand_opt {
			OptionalSubcommands::GenerateShellCompletions(generate_shell_completions_opt) => {
				generate_shell_completions(env!("CARGO_BIN_NAME"), |package_name, command| {
					let shell = generate_shell_completions_opt
						.shell
						.or_else(Shell::from_env)
						.unwrap_or(Shell::Bash);
					generate(shell, command, package_name, &mut io::stdout())
				})
			}
		}
		exit(0)
	}

	gamble(opt).unwrap()
}

fn gamble(opt: Cli) -> io::Result<()> {
	let gamble_strategy = if opt.pass { tcr } else { trc };

	let test_command = reformat_command(opt.test_command);

	if test_command.is_none() {
		log::error!("Can't parse test command");
		exit(1)
	}

	let repository = Repository::new(opt.repository_path.as_path(), opt.dry_run, opt.no_verify);

	repository.run_hook("pre-gamble", &[if opt.pass { "pass" } else { "fail" }])?;

	let command_choice = gamble_strategy(test_command.unwrap());

	match command_choice {
		RepositoryCommandChoice::ShouldCommit => {
			log::info!("commit");

			repository.command(&["add", "--all"])?;

			let base_options = vec!["commit", "--allow-empty-message"];

			let message_options = if opt.message.is_empty() && repository.head_is_failing_ref() {
				vec!["--reuse-message", "HEAD"]
			} else {
				vec!["--message", &opt.message]
			};

			let edit_options = if opt.edit {
				vec!["--edit"]
			} else {
				vec!["--no-edit"]
			};

			let fixup_options = match &opt.fixup {
				Some(commit) => vec!["--fixup", commit],
				None => vec![],
			};

			let squash_options = match &opt.squash {
				Some(commit) => vec!["--squash", commit],
				None => vec![],
			};

			let amend_options = if repository.head_is_failing_ref() {
				vec!["--amend"]
			} else {
				vec![]
			};

			let no_verify_options = if opt.no_verify {
				vec!["--no-verify"]
			} else {
				vec![]
			};

			repository.command(
				&[
					base_options,
					message_options,
					edit_options,
					fixup_options,
					squash_options,
					amend_options,
					no_verify_options,
				]
				.concat(),
			)?;

			if opt.fail {
				repository.command(&["update-ref", "refs/gamble-is-failing", "HEAD"])?;
			}

			repository.run_hook(
				"post-gamble",
				&[
					if opt.pass { "pass" } else { "fail" },
					if opt.pass { "pass" } else { "fail" },
				],
			)?;

			log::info!("committed");
			println!("{}", "Committed!".blue().bold());
		}
		RepositoryCommandChoice::ShouldRevert => {
			log::info!("revert");

			repository.command(&["reset", "--hard"])?;

			repository.run_hook(
				"post-gamble",
				&[
					if opt.pass { "pass" } else { "fail" },
					if opt.pass { "fail" } else { "pass" },
				],
			)?;

			log::info!("reverted");
			println!("{}", "Reverted!".red().bold());
		}
	};

	Ok(())
}

fn reformat_command(command: Vec<String>) -> Option<Vec<String>> {
	if command.len() == 1 {
		shlex::split(command[0].as_str())
	} else {
		Some(command)
	}
}
