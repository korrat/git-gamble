use clap::ArgAction;
use clap::ArgGroup;
use clap::Args;
use clap::Parser;
use clap::Subcommand;
use clap_complete::Shell;
use std::path::PathBuf;

#[derive(Args, Debug)]
pub struct GenerateShellCompletions {
	#[arg(
		value_enum,
		long_help(
			"Put generated file here :\n".to_string() +
			"* Fish https://fishshell.com/docs/current/completions.html#where-to-put-completions\n" +
			"* Others shells ; Don't know, MR are welcome"
		)
	)]
	pub shell: Option<Shell>,
}

#[derive(Subcommand, Debug)]
pub enum OptionalSubcommands {
	GenerateShellCompletions(GenerateShellCompletions),
}

#[derive(Parser, Debug)]
#[command(
	version,
	infer_subcommands = true,
	subcommand_negates_reqs = true,
	group = ArgGroup::new("gambling").required(true),
	about(
		"Blend TDD (Test Driven Development) + TCR (`test && commit || revert`) to make sure to develop\n".to_string() +
		"the right thing 😌, baby step by baby step 👶🦶"
	),
	after_help(
		"Any contributions (feedback, bug report, merge request ...) are welcome\n".to_string() +
		"https://pinage404.gitlab.io/git-gamble/contributing/index.html"
	)
)]
pub struct Cli {
	// subcommands
	#[command(subcommand)]
	pub optional_subcommands: Option<OptionalSubcommands>,

	// flags
	/// Gamble that tests should pass
	#[arg(
		short = 'g',
		long,
		group = "gambling",
		visible_aliases = &["green", "refactor"],
		display_order = 1,
	)]
	pub pass: bool,
	/// Gamble that tests should fail
	#[arg(
		short = 'r',
		long,
		group = "gambling",
		visible_alias = "red",
		display_order = 1
	)]
	pub fail: bool,

	/// Do not make any changes
	#[arg(short = 'n', long)]
	pub dry_run: bool,

	/// Do not run git hooks
	#[arg(long)]
	pub no_verify: bool,

	// options
	/// Repository path
	// reflect `git -C <repository-path>`
	#[arg(short = 'C', long, default_value = ".")]
	pub repository_path: PathBuf,

	/// Commit's message
	#[arg(short = 'm', long, default_value = "")]
	pub message: String,

	/// Open editor to edit commit's message
	#[arg(short = 'e', long)]
	pub edit: bool,

	/// Fixes up commit
	#[arg(long)]
	pub fixup: Option<String>,

	/// Construct a commit message for use with `rebase --autosquash`
	#[arg(long)]
	pub squash: Option<String>,

	// rest arguments
	/// The command to execute to know the result
	#[arg(
		env = "GAMBLE_TEST_COMMAND",
		action(ArgAction::Append),
		last = true,
		required = true
	)]
	pub test_command: Vec<String>,
}

#[test]
fn verify_app() {
	use clap::CommandFactory;

	Cli::command().debug_assert()
}
