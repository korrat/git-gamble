use std::process::Command;

#[derive(Debug, PartialEq, Eq)]
pub enum RepositoryCommandChoice {
	ShouldCommit,
	ShouldRevert,
}

pub fn tcr(command: Vec<String>) -> RepositoryCommandChoice {
	let status = Command::new(&command[0])
		.args(&command[1..])
		.status()
		.expect("failed to run tests");
	if status.success() {
		RepositoryCommandChoice::ShouldCommit
	} else {
		RepositoryCommandChoice::ShouldRevert
	}
}

pub fn trc(command: Vec<String>) -> RepositoryCommandChoice {
	let status = Command::new(&command[0])
		.args(&command[1..])
		.status()
		.expect("failed to run tests");
	if status.success() {
		RepositoryCommandChoice::ShouldRevert
	} else {
		RepositoryCommandChoice::ShouldCommit
	}
}

#[cfg(test)]
mod tcr {
	use super::*;

	#[test]
	fn should_commit_when_command_exited_with_a_zero_code() {
		assert_eq!(
			tcr(vec!(String::from("true"))),
			RepositoryCommandChoice::ShouldCommit
		)
	}

	#[test]
	fn should_revert_when_command_exited_with_a_non_zero_code() {
		assert_eq!(
			tcr(vec!(String::from("false"))),
			RepositoryCommandChoice::ShouldRevert
		)
	}
}

#[cfg(test)]
mod trc {
	use super::*;

	#[test]
	fn should_revert_when_command_exited_with_a_zero_code() {
		assert_eq!(
			trc(vec!(String::from("true"))),
			RepositoryCommandChoice::ShouldRevert
		)
	}

	#[test]
	fn should_commit_when_command_exited_with_a_non_zero_code() {
		assert_eq!(
			trc(vec!(String::from("false"))),
			RepositoryCommandChoice::ShouldCommit
		)
	}
}
