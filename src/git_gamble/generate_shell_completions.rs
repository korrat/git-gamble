use clap::CommandFactory;

use super::cli::Cli;

pub fn generate_shell_completions<Generator>(package_name: &str, shell_generator: Generator)
where
	Generator: Fn(&str, &mut clap::Command),
{
	let mut command = Cli::command();

	shell_generator(package_name, &mut command)
}
