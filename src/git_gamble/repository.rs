use std::borrow::Cow;
use std::io;
#[cfg(unix)]
use std::os::unix::fs::MetadataExt;
use std::path::Path;
use std::path::PathBuf;
use std::process::Command;

pub struct Repository {
	path: PathBuf,
	dry_run: bool,
	no_verify: bool,
}

impl Repository {
	pub fn new(path: &Path, dry_run: bool, no_verify: bool) -> Repository {
		Repository {
			path: path.to_path_buf(),
			dry_run,
			no_verify,
		}
	}

	pub fn path(&self) -> &Path {
		Path::new(&self.path)
	}

	fn shell_command(&self, program: &str, args: &[&str]) -> io::Result<()> {
		let executed_command = &format!(
			"`{} {}` in {}",
			program,
			args.join(" "),
			self.path.display()
		);

		if self.dry_run {
			log::info!("Would execute {}", executed_command);

			return Ok(());
		}

		let help_message = &format!("Failed to execute {}", executed_command);
		let status = Command::new(program)
			.current_dir(self.path())
			.args(args)
			.status()
			.expect(help_message);

		log::info!(
			"{} exited with {}",
			executed_command,
			status.code().unwrap()
		);

		if status.success() {
			Ok(())
		} else {
			Err(io::Error::new(
				io::ErrorKind::Other,
				help_message.to_string(),
			))
		}
	}

	pub fn command(&self, args: &[&str]) -> io::Result<()> {
		self.shell_command("git", args)
	}

	pub fn run_hook(&self, hook_name: &str, args: &[&str]) -> io::Result<()> {
		let hooks_path = self
			.query(&["config", "core.hooksPath"])
			.map(|hooks_path_output| Cow::from(PathBuf::new().join(hooks_path_output.trim())))
			.unwrap_or_else(|_| Cow::from(Path::new(".git").join("hooks")));

		let hook_path = hooks_path.join(hook_name);

		if !hook_path.exists() {
			return Ok(());
		}

		#[cfg(unix)]
		let mode = hook_path.metadata().unwrap().mode();
		#[cfg(unix)]
		if mode & 0o111 == 0 {
			return Ok(());
		}

		if self.no_verify {
			log::info!("Would execute {} hook", hook_name);

			return Ok(());
		}

		self.shell_command(
			"sh",
			&[vec![hook_path.to_str().unwrap_or_default()], args.to_vec()].concat(),
		)
	}

	fn query(&self, args: &[&str]) -> io::Result<String> {
		let query = &format!("`git {}` in {}", args.join(" "), self.path.display());
		let help_message = &format!("Failed to execute {}", query);
		let cmd = Command::new("git")
			.current_dir(self.path())
			.args(args)
			.output()
			.expect(help_message);

		log::info!("query {} exited with {}", query, cmd.status.code().unwrap(),);

		if cmd.status.success() {
			Ok(String::from_utf8(cmd.stdout).unwrap())
		} else {
			Err(io::Error::new(
				io::ErrorKind::Other,
				help_message.to_string(),
			))
		}
	}

	fn get_commit_hash_for(&self, revision: &str) -> io::Result<String> {
		self.query(&["rev-parse", revision])
	}

	pub fn head_is_failing_ref(&self) -> bool {
		let head_commit_hash = self.get_commit_hash_for("HEAD");
		let ref_is_failing_commit_hash = self.get_commit_hash_for("gamble-is-failing");

		match (head_commit_hash, ref_is_failing_commit_hash) {
			(Ok(head_commit_hash), Ok(ref_is_failing_commit_hash)) => {
				head_commit_hash == ref_is_failing_commit_hash
			}
			_ => false,
		}
	}
}
