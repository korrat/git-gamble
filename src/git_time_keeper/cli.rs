use clap::Parser;

pub const DEFAULT_TIMEOUT_COMMAND: &str = "git reset --hard";

#[derive(clap::Args, PartialEq)]
pub struct Args {
	#[arg(long, env = "TIME_KEEPER_MAXIMUM_ITERATION_DURATION")]
	pub iteration_duration: u16,

	#[arg(default_value = DEFAULT_TIMEOUT_COMMAND)]
	pub timeout_command: String,
}

#[derive(Parser, PartialEq)]
#[command(
	version,
	about = "git-time-keeper is a tool that helps to take baby steps 👶🦶",
	after_help(
		"Any contributions (feedback, bug report, merge request ...) are welcome\n".to_string() +
		"https://pinage404.gitlab.io/git-gamble/contributing/index.html"
	)
)]
pub enum Cli {
	/// Start the time keeper in background
	Start(Args),

	#[command(
		about = "The actual time keeper that is ran in background",
		long_about(
			"The actual time keeper that is ran in background\n".to_string() +
			"\n" +
			"For internal use only"
		)
	)]
	Background(Args),

	/// Stop the time keeper
	Stop,
}

#[test]
fn verify_app() {
	use clap::CommandFactory;

	Cli::command().debug_assert()
}
