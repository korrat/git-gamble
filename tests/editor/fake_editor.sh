#!/usr/bin/env sh

# fail on first error
set -o errexit
set -o nounset

FILE_CONTENT="${1}"
FILE_TO_EDIT="${2}"

echo "${FILE_CONTENT}" >"${FILE_TO_EDIT}"
