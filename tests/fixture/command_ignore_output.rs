use std::process::Command;
use std::process::Stdio;

pub trait CommandIgnoreOutputExt {
	fn ignore_output(&mut self) -> &mut Self;
}

impl CommandIgnoreOutputExt for Command {
	fn ignore_output(&mut self) -> &mut Self {
		self.stdout(Stdio::null()).stderr(Stdio::null())
	}
}
