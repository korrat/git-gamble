use std::env;
use std::path::Path;

fn path_with_binaries() -> [(String, String); 1] {
	let binaries_path = option_env!("CARGO_BIN_EXE_git-time-keeper")
		.map(Path::new)
		.and_then(|path| path.parent())
		.expect("can't get git-time-keeper binary path");

	let path = env::var("PATH").expect("can't get PATH environment variable");

	let path_with_binaries = format!("{}:{}", binaries_path.display(), path);

	[("PATH".to_string(), path_with_binaries)]
}

pub trait CommandWithCrateBinariesInThePathExt {
	fn with_crate_binaries_in_the_path(&mut self) -> &mut Self;
}

impl CommandWithCrateBinariesInThePathExt for std::process::Command {
	fn with_crate_binaries_in_the_path(&mut self) -> &mut Self {
		self.envs(path_with_binaries())
	}
}

impl CommandWithCrateBinariesInThePathExt for assert_cmd::Command {
	fn with_crate_binaries_in_the_path(&mut self) -> &mut Self {
		self.envs(path_with_binaries())
	}
}
