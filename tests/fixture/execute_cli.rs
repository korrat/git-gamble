use assert_cmd::assert::Assert;
use assert_cmd::Command;
use std::str;

pub fn execute_cli(bin: &str) -> Command {
	Command::cargo_bin(bin).unwrap()
}

pub fn execute_cli_with(bin: &str, args: &[&str]) -> Assert {
	log::info!("{} {}", bin, args.join(" "));
	execute_cli(bin).args(args).assert()
}
