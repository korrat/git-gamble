use assert_cmd::assert::Assert;
use speculoos::assert_that;
use speculoos::prelude::StrAssertions;
use std::str;

pub fn assert_smoke_in_output_contains(command: Assert, subject: &str) {
	let output = str::from_utf8(&command.get_output().stdout)
		.expect("stdout can't be parsed to UTF-8")
		.to_string();
	assert_that(&output).contains(subject);
}

pub fn assert_smoke_committed(command: Assert) {
	assert_smoke_in_output_contains(command, "Committed");
}

pub fn assert_smoke_reverted(command: Assert) {
	assert_smoke_in_output_contains(command, "Reverted");
}
