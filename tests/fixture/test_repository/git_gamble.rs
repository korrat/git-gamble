use assert_cmd::assert::Assert;
use std::str;

use crate::fixture::execute_cli;
use crate::fixture::TestRepository;

impl TestRepository {
	pub fn gamble_with(&self, args: &[&str]) -> Assert {
		log::info!("git-gamble {}", args.join(" "));
		execute_cli("git-gamble")
			.current_dir(self.path())
			.args(args)
			.assert()
	}

	pub fn gamble_editor(&self, args: &[&str], editor_content_to_file: &str) -> Assert {
		let editor = format!(
			"{}/tests/editor/fake_editor.sh {}",
			env!("CARGO_MANIFEST_DIR"),
			editor_content_to_file
		);
		log::info!("GIT_EDITOR=\"{}\" git-gamble {}", editor, args.join(" "));
		execute_cli("git-gamble")
			.current_dir(self.path())
			.args(args)
			.env("GIT_EDITOR", editor)
			.assert()
	}
}
