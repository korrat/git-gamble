use assert_cmd::assert::Assert;
use std::fs;
use std::io;
use std::path::PathBuf;
use std::str;

use crate::fixture::execute_cli;
use crate::fixture::CommandWithCrateBinariesInThePathExt;
use crate::fixture::TestRepository;

impl TestRepository {
	pub fn start_time_keeper_for(&self, iteration_duration: &str) -> Assert {
		log::info!(
			"TIME_KEEPER_MAXIMUM_ITERATION_DURATION={} git-time-keeper start",
			iteration_duration
		);
		execute_cli("git-time-keeper")
			.with_crate_binaries_in_the_path()
			.current_dir(self.path())
			.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", iteration_duration)
			.args(["start"])
			.assert()
	}

	pub fn get_time_keeper_lock_files_folder(&self) -> PathBuf {
		self.path().join(".git").join("git-time-keeper.lock")
	}

	pub fn count_lock_files(&self) -> io::Result<usize> {
		let lock_file_folder = self.get_time_keeper_lock_files_folder();

		if !lock_file_folder.exists() {
			return Ok(0);
		}

		let actual_number_of_lock_files = fs::read_dir(&lock_file_folder)?
			.filter_map(|entry| entry.ok())
			.count();

		Ok(actual_number_of_lock_files)
	}
}
