use std::io;
use test_log::test;

use crate::fixture::TestRepository;

#[test]
fn runs_hooks_when_not_passing_no_verify() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;
	test_repository.add_hook("pre-commit", "false")?;

	let commit_message = "some message";
	let command =
		test_repository.gamble_with(&["--pass", "--message", commit_message, "--", "true"]);

	command.failure();
	assert!(test_repository.is_dirty());
	assert!(test_repository.has_no_new_commit());
	Ok(())
}

#[test]
fn does_not_run_hooks_when_passing_no_verify() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;
	test_repository.add_hook("pre-commit", "false")?;

	let commit_message = "some message";
	let command = test_repository.gamble_with(&[
		"--pass",
		"--message",
		commit_message,
		"--no-verify",
		"--",
		"true",
	]);

	command.success();
	assert!(test_repository.is_clean());
	assert!(test_repository.has_new_commit());
	Ok(())
}
