use std::io;
use test_case::test_case;

use crate::fixture::smoke_assertion::assert_smoke_committed;
use crate::fixture::TestRepository;

#[test_case("--dry-run")]
#[test_case("-n")]
#[test_log::test]
fn dry_run(flag: &str) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = test_repository
		.gamble_with(&[flag, "--pass", "--", "true"])
		.success();

	assert!(test_repository.is_dirty());
	assert!(test_repository.changes_remain_in_the_working_file());
	assert!(test_repository.has_no_new_commit());
	assert_smoke_committed(command);
	Ok(())
}
