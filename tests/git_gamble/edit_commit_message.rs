use speculoos::assert_that;
use std::io;
use test_case::test_case;

use crate::fixture::TestRepository;

#[test_case("-e")]
#[test_case("--edit")]
#[test_log::test]
fn open_editor_when_committing(flag: &str) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let commit_message = "message";
	let command = test_repository.gamble_editor(&["--pass", flag, "--", "true"], commit_message);

	command.success();
	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}
