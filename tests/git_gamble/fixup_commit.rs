use speculoos::assert_that;
use std::io;

use crate::fixture::TestRepository;

#[test_log::test]
fn fixup_commit() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = test_repository.gamble_with(&["--pass", "--fixup", "HEAD", "--", "true"]);

	command.success();
	let commit_message = "fixup! first commit";
	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_log::test]
fn fixup_commit_with_commit_search() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = test_repository.gamble_with(&["--pass", "--fixup", ":/initial", "--", "true"]);

	command.success();
	let commit_message = "fixup! initial commit";
	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}
