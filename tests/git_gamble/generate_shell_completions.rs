use std::io;
use test_log::test;

use crate::fixture::assert_smoke_in_output_contains;
use crate::fixture::execute_cli;
use crate::fixture::execute_cli_with;

#[test]
fn for_fish() -> io::Result<()> {
	let command = execute_cli_with("git-gamble", &["generate-shell-completions", "fish"]).success();

	assert_smoke_in_output_contains(
		command,
		"complete -c git-gamble -n \"__fish_use_subcommand\" -s h -l help -d 'Print help'",
	);
	Ok(())
}

#[test]
fn for_bash() -> io::Result<()> {
	let command = execute_cli_with("git-gamble", &["generate-shell-completions", "bash"]).success();

	assert_smoke_in_output_contains(
		command,
		"complete -F _git-gamble -o nosort -o bashdefault -o default git-gamble",
	);
	Ok(())
}

#[test]
fn allow_to_call_subcommand_without_typing_the_entire_name() -> io::Result<()> {
	let command = execute_cli_with("git-gamble", &["generate", "fish"]).success();

	assert_smoke_in_output_contains(
		command,
		"complete -c git-gamble -n \"__fish_use_subcommand\" -s h -l help -d 'Print help'",
	);
	Ok(())
}

#[test]
fn default_to_bash() -> io::Result<()> {
	let command = execute_cli_with("git-gamble", &["generate-shell-completions"]).success();

	assert_smoke_in_output_contains(
		command,
		"complete -F _git-gamble -o nosort -o bashdefault -o default git-gamble",
	);
	Ok(())
}

#[test]
fn for_the_current_shell() -> io::Result<()> {
	let command = execute_cli("git-gamble")
		.env("SHELL", "fish")
		.args(["generate-shell-completions"])
		.assert()
		.success();

	assert_smoke_in_output_contains(
		command,
		"complete -c git-gamble -n \"__fish_use_subcommand\" -s h -l help -d 'Print help'",
	);
	Ok(())
}
