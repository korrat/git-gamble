use std::io;

use crate::fixture::TestHook;
use crate::fixture::TestRepository;

mod pre_gamble_hook_pass_gambling_option_to_hook {
	use super::*;

	#[test_log::test]
	fn when_gambling_pass() -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;
		let test_hook = TestHook::init(&test_repository, "pre-gamble")?;

		let command = test_repository.gamble_with(&["--pass", "--", "true"]);

		command.success();
		test_hook.assert_that_hook_has_been_run_with(".git/hooks/pre-gamble pass");
		Ok(())
	}

	#[test_log::test]
	fn when_gambling_fail() -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;
		let test_hook = TestHook::init(&test_repository, "pre-gamble")?;

		let command = test_repository.gamble_with(&["--fail", "--", "true"]);

		command.success();
		test_hook.assert_that_hook_has_been_run_with(".git/hooks/pre-gamble fail");
		Ok(())
	}
}

mod post_gamble_hook_pass_gambling_option_and_test_result_to_hook {
	use super::*;

	#[test_log::test]
	fn when_gambling_fail_and_test_fail() -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;
		let test_hook = TestHook::init(&test_repository, "post-gamble")?;

		let command = test_repository.gamble_with(&["--fail", "--", "false"]);

		command.success();
		test_hook.assert_that_hook_has_been_run_with(".git/hooks/post-gamble fail fail");
		Ok(())
	}

	#[test_log::test]
	fn when_gambling_pass_and_test_pass() -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;
		let test_hook = TestHook::init(&test_repository, "post-gamble")?;

		let command = test_repository.gamble_with(&["--pass", "--", "true"]);

		command.success();
		test_hook.assert_that_hook_has_been_run_with(".git/hooks/post-gamble pass pass");
		Ok(())
	}

	#[test_log::test]
	fn when_gambling_fail_but_test_pass() -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;
		let test_hook = TestHook::init(&test_repository, "post-gamble")?;

		let command = test_repository.gamble_with(&["--fail", "--", "true"]);

		command.success();
		test_hook.assert_that_hook_has_been_run_with(".git/hooks/post-gamble fail pass");
		Ok(())
	}

	#[test_log::test]
	fn when_gambling_pass_but_test_fail() -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;
		let test_hook = TestHook::init(&test_repository, "post-gamble")?;

		let command = test_repository.gamble_with(&["--pass", "--", "false"]);

		command.success();
		test_hook.assert_that_hook_has_been_run_with(".git/hooks/post-gamble pass fail");
		Ok(())
	}
}
