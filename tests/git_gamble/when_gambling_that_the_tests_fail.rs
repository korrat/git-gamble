use std::io;
use std::str;

use crate::fixture::smoke_assertion::assert_smoke_committed;
use crate::fixture::smoke_assertion::assert_smoke_reverted;
use crate::fixture::TestRepository;
use crate::fixture::THIRD_CONTENT;

mod when_gambling_that_the_tests_fail_that_should {
	use super::*;
	use test_case::test_case;

	#[test_case("--fail")]
	#[test_case("--red")]
	#[test_case("-r")]
	#[test_log::test]
	fn commit_when_tests_fail(flag: &str) -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;

		let command = test_repository
			.gamble_with(&[flag, "--", "false"])
			.success();

		assert!(test_repository.is_clean());
		assert!(test_repository.changes_remain_in_the_working_file());
		assert!(test_repository.has_new_commit());
		assert!(test_repository.head_is_failing_ref());
		assert_smoke_committed(command);
		Ok(())
	}

	#[test_case("--fail")]
	#[test_case("--red")]
	#[test_case("-r")]
	#[test_log::test]
	fn revert_when_tests_pass(flag: &str) -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;

		let command = test_repository.gamble_with(&[flag, "--", "true"]).success();

		assert!(test_repository.is_clean());
		assert!(test_repository.changes_reverted_in_the_working_file());
		assert!(test_repository.has_no_new_commit());
		assert_smoke_reverted(command);
		Ok(())
	}

	#[test_case("--fail")]
	#[test_case("--red")]
	#[test_case("-r")]
	#[test_log::test]
	fn amend_commit_when_tests_fail_and_the_last_test_failed(flag: &str) -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;

		test_repository
			.gamble_with(&[flag, "--", "false"])
			.success();
		let previous_commit_hash = test_repository.get_commit_hash_for("HEAD");

		test_repository.make_working_file_dirty_with(THIRD_CONTENT)?;

		test_repository
			.gamble_with(&[flag, "--", "false"])
			.success();

		assert!(test_repository.is_clean());
		assert!(test_repository.working_file_is_equal_to(THIRD_CONTENT));
		assert!(test_repository.has_new_commit());
		assert!(test_repository.head_is_failing_ref());
		assert!(test_repository.head_is_different_from(previous_commit_hash));
		Ok(())
	}
}
