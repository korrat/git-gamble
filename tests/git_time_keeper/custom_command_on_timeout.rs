use assert_cmd::assert::OutputAssertExt;
use speculoos::assert_that;
use speculoos::path::PathAssertions;
use std::io;
use std::process::Command;
use std::thread;
use std::time::Duration;
use test_case::test_case;

use crate::fixture::delay_to_ensure_that_the_file_system_is_synced;
use crate::fixture::CommandIgnoreOutputExt;
use crate::fixture::CommandWithCrateBinariesInThePathExt;
use crate::fixture::TestRepository;

#[test]
fn can_pass_command_to_execute_at_end_of_iteration() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start", "false"])
		.ignore_output()
		.assert()
		.success();

	Ok(())
}

#[test]
fn command_is_executed_at_end_of_iteration() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;
	let indicator = test_repository.path().join("indicator");

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start", format!("touch {}", indicator.display()).as_str()])
		.ignore_output()
		.assert()
		.success();

	thread::sleep(Duration::from_secs(1));

	delay_to_ensure_that_the_file_system_is_synced();
	assert_that!(&indicator).exists();
	Ok(())
}

#[test]
fn nothing_is_executed_before_the_end_of_iteration() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;
	let indicator = test_repository.path().join("indicator");

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start", format!("touch {}", indicator.display()).as_str()])
		.ignore_output()
		.assert()
		.success();

	delay_to_ensure_that_the_file_system_is_synced();
	assert_that!(&indicator).does_not_exist();
	Ok(())
}

#[test]
fn do_not_run_the_default_command_when_a_custom_is_given() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start", "true"])
		.ignore_output()
		.assert()
		.success();

	thread::sleep(Duration::from_secs(1));

	delay_to_ensure_that_the_file_system_is_synced();
	assert!(test_repository.is_dirty());
	assert!(test_repository.changes_remain_in_the_working_file());
	Ok(())
}

#[test_case("" ; "totally empty")]
#[test_case("   " ; "full of spaces")]
fn empty_command_fallbacks_to_the_default_command(timeout_command: &str) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start", timeout_command])
		.ignore_output()
		.assert()
		.success();

	thread::sleep(Duration::from_secs(1));

	delay_to_ensure_that_the_file_system_is_synced();
	assert!(test_repository.is_clean());
	assert!(test_repository.changes_reverted_in_the_working_file());
	Ok(())
}
