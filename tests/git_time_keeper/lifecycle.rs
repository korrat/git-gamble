use std::io;
use std::process::Command;
use std::thread;
use std::time::Duration;

use crate::fixture::delay_to_ensure_that_the_file_system_is_synced;
use crate::fixture::execute_cli;
use crate::fixture::CommandIgnoreOutputExt;
use crate::fixture::CommandWithCrateBinariesInThePathExt;
use crate::fixture::TestRepository;

#[test]
fn do_nothing_when_stopping_without_started_before() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["stop"])
		.assert()
		.success();

	thread::sleep(Duration::from_secs(1));

	assert!(test_repository.changes_remain_in_the_working_file());
	Ok(())
}

#[test]
fn do_nothing_when_starting_and_stopping_before_the_end_of_the_iteration() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "2")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	delay_to_ensure_that_the_file_system_is_synced();
	delay_to_ensure_that_the_file_system_is_synced();

	execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "2")
		.args(["stop"])
		.assert();

	thread::sleep(Duration::from_secs(2));

	delay_to_ensure_that_the_file_system_is_synced();
	delay_to_ensure_that_the_file_system_is_synced();
	assert!(test_repository.changes_remain_in_the_working_file());
	Ok(())
}

#[test]
fn do_nothing_the_second_time_by_starting_twice() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "2")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	delay_to_ensure_that_the_file_system_is_synced();

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	thread::sleep(Duration::from_secs(1));

	delay_to_ensure_that_the_file_system_is_synced();
	assert!(test_repository.changes_remain_in_the_working_file());
	Ok(())
}
