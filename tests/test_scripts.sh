#!/usr/bin/env sh

set -o errexit
set -o nounset

./tests/editor/test_fake_editor.sh

./script/functions/tests/test_skip_lines_before_pattern.sh

./script/functions/tests/test_skip_lines_after_pattern.sh

./script/test_display_file_between_bounds.sh

./script/tests/test_git-time-keeper.sh

./ci/tests/test_display_changelog_of_version.sh

./.config/git/hooks/tests/test_post_gamble_real_time_collaboration.sh

./.config/git/hooks/tests/test_post_gamble_assistant.sh
